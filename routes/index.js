var express = require('express');
var router = express.Router();
const excelData = require('../src/excelData');
/* GET home page. */
router.get('/', async function (req, res, next) {
  await excelData.fullFlow();
  res.render('index', { title: 'Express' });
});

module.exports = router;
