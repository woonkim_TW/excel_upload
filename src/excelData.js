const Client = require('ssh2-sftp-client');
const Excel = require('exceljs');
const _ = require('lodash');
const fs = require('fs');
const moment = require('moment');
const path = require('path');

const tempFilePath = path.join(__dirname, '..', 'temp');
const sftpHost = '127.0.0.1';
const sftpPort = '22';
const sftpUsername = 'kim';
const sftpPassword = 'abcd1234';

const saveExcel = async (fileName, data) => {
  const filePath = path.join(tempFilePath, fileName);
  const workbook = new Excel.Workbook();
  const sheet = workbook.addWorksheet('Data');
  for (let o of data) {
    sheet.addRow(o);
  }
  await workbook.csv.writeFile(filePath);
};

const getDailyReport = async () => {
  return [
    {
      id: 1,
      number: 1,
      remainingQuota: 6,
      enrichId: '1111',
      date: new moment().format()
    },
    {
      id: 2,
      number: 2,
      remainingQuota: 6,
      enrichId: '2222',
      date: new moment().format()
    },
    {
      id: 3,
      number: 3,
      remainingQuota: 6,
      enrichId: '3333',
      date: new moment().format()
    }
  ];
};

const uploadFile = async fileName => {
  let sftp = new Client();
  const src = path.join(tempFilePath, fileName);
  const dst = path.join('/', fileName);
  try {
    await sftp.connect({
      host: sftpHost,
      port: sftpPort,
      username: sftpUsername,
      password: sftpPassword
    });
    await sftp.put(src, dst);
  } catch (e) {
    throw new Error('SFTP error');
  } finally {
    sftp.end();
  }
};

const deleteFile = async fileName => {
  try {
    const fileToBeDeleted = path.join(tempFilePath, fileName);
    fs.unlinkSync(fileToBeDeleted);
  } catch (err) {
    throw new Error('Delete file error');
  }
};

const processFile = async data => {
  /**
   * from this -> [{aaa...}]
   * to this -> [[row1],[row2],...]
   *
   */

  let expectedResp = [];
  let firstData = data[0];
  expectedResp.push(_.keys(firstData));
  for (let o of data) {
    expectedResp.push(_.values(o));
  }
  return expectedResp;
};

/**
 * axios get data
 * save to csv
 * put to sftp
 * delete temp file
 */

const fullFlow = async () => {
  let dailyReport = await getDailyReport();

  let fileDateFormat = new moment().format('YYYYMMDD');
  let fileTimeFormat = new moment().format('HHmmss');
  const fileName = `TST.TPPASS.BATCH.D${fileDateFormat}.T${fileTimeFormat}.csv`;
  let dataToBeSaved = await processFile(dailyReport);
  await saveExcel(fileName, dataToBeSaved);
  await uploadFile(fileName);
  await deleteFile(fileName);
};

module.exports = {
  fullFlow
};
